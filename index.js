const http = require('http');

const port = 3000;

const server = http.createServer(function(request, response) => {
	response.writeHead(200, {'Content-type': 'text/plain'})
	response.end("You are at the login page")

	
	if(request.url == '/login') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('login')
	}

	
	else if(request.url != '/login') {
		response.writeHead(404,{'Content-Type':'text/plain'});
		response.end('Page not found')
	}

	else if(request.url == '/about'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to about page')
	} else {
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('Page not found')

	}


}).listen(port);

//When server is running, console will print the message:
console.log(`Server running at localhost: {$port}`);
